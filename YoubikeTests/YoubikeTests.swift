//
//  YoubikeTests.swift
//  YoubikeTests
//
//  Created by johnlin on 2021/4/26.
//

import XCTest
import Combine
@testable import Youbike

class YoubikeTests: XCTestCase {
    var viewModel: YoubikeViewModel!
    var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = YoubikeViewModel()
        cancellables = []
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAPIRequest() {
        let promise = expectation(description: "發起 request 失敗")
        APIService.path = ""
        var resultError: String?

        viewModel.fetchData(state: .loading) { (success, error) in
            resultError = error
            promise.fulfill()
        }
        wait(for: [promise], timeout: 20)

        XCTAssertNotNil(resultError)
    }

    func testSearch() {
        let emptyText = ""
        let searchText = "中山"
        
        viewModel.searchText(keyword: emptyText)
        viewModel.searchText(keyword: searchText)
        
        XCTAssertTrue(viewModel.filterData.isEmpty)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
//            viewModel.reload(state: .loading)
        }
    }

}
