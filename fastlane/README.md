fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios lint
```
fastlane ios lint
```

### ios register_new_device
```
fastlane ios register_new_device
```
Update and Register Devices
### ios register_enterpris_new_device
```
fastlane ios register_enterpris_new_device
```
Update and Register Devices Other Device
### ios refresh_development_profiles
```
fastlane ios refresh_development_profiles
```
Refresh Certificates And Profiles
### ios refresh_distribution_profiles
```
fastlane ios refresh_distribution_profiles
```
Refresh Certificates And Profiles and Op
### ios download_all_certificates
```
fastlane ios download_all_certificates
```

### ios download_development_certificates
```
fastlane ios download_development_certificates
```

### ios download_adhoc_certificates
```
fastlane ios download_adhoc_certificates
```

### ios download_distribution_certificates
```
fastlane ios download_distribution_certificates
```

### ios dev_beta
```
fastlane ios dev_beta
```
打包 development ipa
### ios test_beta
```
fastlane ios test_beta
```
打包 ad-hoc ipa
### ios release_beta
```
fastlane ios release_beta
```
打包 appstore ipa
### ios tests
```
fastlane ios tests
```

### ios submit_appstore_review
```
fastlane ios submit_appstore_review
```

### ios beta
```
fastlane ios beta
```
Submit a new Beta Build to Apple TestFlight

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
