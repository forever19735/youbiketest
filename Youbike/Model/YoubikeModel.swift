//
//  YoubikeModel.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

struct YoubikeModel: Decodable {
    let retCode: Int
    let retVal: [String: YoubikeInfo]
}

struct YoubikeInfo: Decodable {
    let sno: String
    let sna: String
    let tot: String
    let sbi: String
    let sarea: String
    let mday: String
    let lat: String
    let lng:String
    let ar: String
    let bemp: String
}
