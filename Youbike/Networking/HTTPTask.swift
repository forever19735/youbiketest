//
//  HttpTask.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
    case requestPlain
    case requestParameters(bodyParameters: CustomParameters?, urlParameters: CustomParameters?)
    case requestParametersAndHeaders(bodyParameters: CustomParameters?, urlParameters: CustomParameters?, additionHeaders: HTTPHeaders?)
    case requestHeaders(additionHeaders: HTTPHeaders?)
}
