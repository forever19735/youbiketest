//
//  EndPointType.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

public enum NetworkError: String, Error {
    case parametersNil = "Parameters were nil."
    case encodingFailed = "Paramter encoding failed."
    case missingURL = "URL is nil."
}

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
    var parameters: CustomParameters? { get }
}
