//
//  HttpMethod.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
