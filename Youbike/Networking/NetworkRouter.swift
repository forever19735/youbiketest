//
//  NetworkRouter.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void
protocol NetworkRouter: AnyObject {
    associatedtype EndPoint: EndPointType
    func request(_ router: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}
