//
//  BaseViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import UIKit
import Combine

class BaseViewController: UIViewController {
    var cancellables = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bind()
    }
    
    func setupUI() {
        
    }
    
    func bind() {
        
    }
}
