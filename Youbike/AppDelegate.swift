//
//  AppDelegate.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import UIKit
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var locationManager: CLLocationManager!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = BaseNavigationController(rootViewController: RootViewController())
        self.window?.makeKeyAndVisible()
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        return true
    }
}

extension AppDelegate {
    func startLocationManager() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .denied:
            showAlert()
        default:
            break
        }
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "警告", message: "只有允許在 「使用 App 期間」才能取用位置。", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let changeAction = UIAlertAction(title: "修改設定", style: .default) { (_) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        alert.addAction(okAction)
        alert.addAction(changeAction)
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
}
