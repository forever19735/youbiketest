//
//  ConfigCell.swift
//  Youbike
//
//  Created by 季紅 on 2021/12/6.
//

import Foundation

protocol ConfigCell {
    associatedtype ViewData
    func configure(viewData: ViewData)
    func setupUI()
}
