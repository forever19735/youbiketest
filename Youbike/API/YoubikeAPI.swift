//
//  YoubikeAPI.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

enum YoubikeAPI {
    case getData
}

extension YoubikeAPI: EndPointType {
    var task: HTTPTask {
        return .requestPlain
    }
    
    var parameters: CustomParameters? {
        return nil
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var baseURL: URL {
        guard let url = URL(string: "https://tcgbusfs.blob.core.windows.net") else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .getData:
            return "/blobyoubike/YouBikeTP.json"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
}

