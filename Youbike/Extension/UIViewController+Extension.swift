//
//  UIViewController+Extension.swift
//  Youbike
//
//  Created by 季紅 on 2021/12/7.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func embed(_ viewController: UIViewController, inView view: UIView){
        viewController.willMove(toParent: self)
        viewController.view.frame = view.bounds
        view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    
    func remove(_ viewController: UIViewController, inView view: UIView) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func showLoading(autoHide: Bool = false, delay: TimeInterval = 1.5) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .indeterminate
        if autoHide {
            hud.hide(animated: true, afterDelay: delay)
        }
    }
    
    func hideLoading() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
