//
//  RegisterViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/4/1.
//

import UIKit

class RegisterViewController: BaseViewController {
    let padding = 15
    var userNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "輸入帳號"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "輸入密碼"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    var confirmPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "再次輸入密碼"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton()
        button.setTitle("註冊", for: .normal)
        button.setTitle("無法註冊", for: .disabled)
        button.backgroundColor = .systemBlue
        return button
    }()
    
    var tipLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    let viewModel = RegisterViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func setupUI() {
        self.view.addSubview(userNameTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(confirmPasswordTextField)
        self.view.addSubview(registerButton)
        self.view.addSubview(tipLabel)
        userNameTextField.snp.makeConstraints({
            $0.top.equalToSuperview().offset(100)
            $0.leading.equalToSuperview().offset(padding)
            $0.trailing.equalToSuperview().offset(-padding)
        })
        passwordTextField.snp.makeConstraints({
            $0.top.equalTo(userNameTextField.snp.bottom).offset(padding)
            $0.leading.trailing.equalTo(userNameTextField)
        })
        confirmPasswordTextField.snp.makeConstraints({
            $0.top.equalTo(passwordTextField.snp.bottom).offset(padding)
            $0.leading.trailing.equalTo(userNameTextField)
        })
        registerButton.snp.makeConstraints({
            $0.top.equalTo(confirmPasswordTextField.snp.bottom).offset(padding)
            $0.leading.trailing.equalTo(userNameTextField)
            $0.height.equalTo(40)
        })
        tipLabel.snp.makeConstraints({
            $0.top.equalTo(registerButton.snp.bottom).offset(padding)
            $0.leading.trailing.equalTo(userNameTextField)
        })
    }
    
    override func bind() {
        userNameTextField.publisherForTextChanged()
            .sink { [weak self] (value) in
                self?.viewModel.username = value ?? ""
            }
            .store(in: &cancellables)
        
        passwordTextField.publisherForTextChanged()
            .sink { [weak self] (value) in
                self?.viewModel.password = value ?? ""
            }
            .store(in: &cancellables)
        
        confirmPasswordTextField.publisherForTextChanged()
            .sink { [weak self] (value) in
                self?.viewModel.confirmPassword = value ?? ""
            }
            .store(in: &cancellables)
        
        viewModel.validRegisterPublisher
            .assign(to: \UIButton.isEnabled, on: registerButton)
            .store(in: &cancellables)
        
        NotificationCenter
            .default
            .publisher(for: UITextField.textDidChangeNotification, object: userNameTextField)
            .compactMap({$0.object as? UITextField})
            .map({$0.text})
            .receive(on: DispatchQueue.main)
            .assign(to: \.text, on: userNameTextField)
            .store(in: &cancellables)
    }
}
