//
//  RegisterViewModel.swift
//  Youbike
//
//  Created by johnlin on 2021/4/1.
//

import Foundation
import Combine

class RegisterViewModel {
    @Published var username: String = ""
    @Published var password: String = ""
    @Published var confirmPassword: String = ""
    
    var validRegisterPublisher: AnyPublisher<Bool, Never>{
        return Publishers.CombineLatest3($username, $password, $confirmPassword)
            .map { (username, pwd, pwd2) -> Bool in
                return username.count >= 6 && pwd.count >= 6 && pwd == pwd2 && self.check()
            }.eraseToAnyPublisher()
    }
    
    func check() -> Bool {
        return true
    }
}
