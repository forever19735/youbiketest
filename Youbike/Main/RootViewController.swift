//
//  RootViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/3/18.
//

import UIKit
import Combine

enum MapType: Int {
    case map = 0
    case list = 1
}

class RootViewController: BaseViewController {
    lazy var firstVC: AllLocationViewController = {
        let vc = AllLocationViewController(viewModel: viewModel)
        return vc
    }()
    lazy var secondVC: YoubikeViewController = {
        let vc = YoubikeViewController(viewModel: viewModel)
        return vc
    }()
    lazy var segmentControl: UISegmentedControl = {
        let segment = UISegmentedControl(items: ["地圖", "條列"])
        segment.sizeToFit()
        segment.selectedSegmentTintColor = .systemYellow
        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .normal)
        segment.selectedSegmentIndex = 0
        return segment
    }()
    let viewModel = YoubikeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
        self.embed(self.firstVC, inView: self.view)
    }
    
    override func setupUI() {
        self.navigationItem.titleView = segmentControl
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "刷新", style: .plain, target: self, action: #selector(reload))
    }
    
    override func bind() {
        viewModel.getDataError
            .receive(on: DispatchQueue.main)
            .sink { [weak self]  (value) in
                let alert = UIAlertController(title: "錯誤訊息", message: value, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                self?.present(alert, animated: true, completion: nil)
            }.store(in: &cancellables)
        
        segmentControl
            .publisher(for: .valueChanged)
            .sink { [weak self] _ in
                self?.changeVC()
            }.store(in: &cancellables)
    }
}

extension RootViewController {
    @objc func reload() {
        viewModel.fetchData()
    }
    
    func changeVC() {
        let type = MapType(rawValue: segmentControl.selectedSegmentIndex)
        switch type {
        case .map:
            remove(secondVC, inView: view)
            embed(firstVC, inView: view)
        case .list:
            remove(firstVC, inView: view)
            embed(secondVC, inView: view)
        case .none:
            break
        }
    }
}
