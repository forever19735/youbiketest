//
//  YoubikeViewModel.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation
import Combine

class YoubikeViewModel {
    @Published var data: [YoubikeInfo] = []
    
    @Published var isRefresh: Bool = false
    
    private(set) var getDataError = PassthroughSubject<String, Never>()
    
}

extension YoubikeViewModel {
    func fetchData(isRefresh: Bool = false) {
        self.isRefresh = isRefresh
        NetworkManager.shared.request(YoubikeAPI.getData, model: YoubikeModel.self) { (result) in
            self.isRefresh = false
            switch result {
            case .success(let content):
                self.data = content.retVal.map({$0.value}).sorted { (info1, info2) -> Bool in
                    return info1.mday.stringConvertDate() > info2.mday.stringConvertDate()
                }
            case .fail(let error):
                self.getDataError.send(error)
            }
        }
    }
    
    func refersh() {
        fetchData(isRefresh: true)
    }
}
