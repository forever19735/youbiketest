//
//  YoubikeTableViewCell.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import UIKit

struct YoubikeViewData: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(data.sno)
    }
    
    static func == (lhs: YoubikeViewData, rhs: YoubikeViewData) -> Bool {
        lhs.data.sno == rhs.data.sno
    }
    
    let data: YoubikeInfo
}

class YoubikeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var arLabel: UILabel!
    
    @IBOutlet weak var updateTimeLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
    }
    
    func configCell(viewData: YoubikeViewData) {
        let info = viewData.data
        titleLabel.text = info.sna
        arLabel.text = "可借：\(info.sbi.toInt() ?? 0) 台"
        updateTimeLabel.text = "更新時間：" + info.mday.stringConvertDate().dateConvertString()
    }
}
