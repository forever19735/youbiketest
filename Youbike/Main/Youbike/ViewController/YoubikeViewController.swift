//
//  YoubikeViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import UIKit
import SnapKit

class YoubikeViewController: BaseViewController {
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(cellType: YoubikeTableViewCell.self)

        return tableView
    }()
    
    var refreshControl: UIRefreshControl!
    
    var viewModel = YoubikeViewModel()
    
    private lazy var dataSource = makeDataSource()
    
    init(viewModel: YoubikeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }
    
    override func setupUI() {
        tableView.dataSource = dataSource
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
    }
    
    override func bind() {
        viewModel.$isRefresh
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (value) in
                if !value {
                    self?.refreshControl.endRefreshing()
                }
            }
            .store(in: &cancellables)
        
        refreshControl
            .publisher(for: .valueChanged)
            .sink { [weak self] _ in
                self?.viewModel.refersh()
            }
            .store(in: &cancellables)
        
        viewModel.$data
            .sink { [weak self] value in
                guard let self = self else { return }
                self.initDataSource(with: value)
            }
            .store(in: &cancellables)
    }
}

extension YoubikeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let item = dataSource.itemIdentifier(for: indexPath)
        if let data = item?.data {
            let detailVC = DetailViewController(data: data)
            detailVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension YoubikeViewController {
    enum Section {
        case main
    }
    
    private func makeDataSource() -> UITableViewDiffableDataSource<Section, YoubikeViewData> {
        return UITableViewDiffableDataSource(tableView: tableView) { tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(with: YoubikeTableViewCell.self, for: indexPath)
            cell.configCell(viewData: item)
            return cell
        }
    }
    
    private func initDataSource(with info: [YoubikeInfo], animate: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, YoubikeViewData>()
        snapshot.appendSections([.main])
        
        let youbikeInfo = info.map({YoubikeViewData(data: $0)})
        
        snapshot.appendItems(youbikeInfo, toSection: .main)
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}
