//
//  DetailViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import UIKit
import MapKit
import CoreLocation
import SnapKit

class DetailViewController: BaseViewController {
    var data: YoubikeInfo?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        let cells = [DetailMapTableViewCell.self,
                     DetailInfoTableViewCell.self]
        tableView.register(cellTypes: cells)
        
        return tableView
    }()
    
    private lazy var dataSource = makeDataSource()
    
    init(data: YoubikeInfo) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initDataSource()
    }
    
    override func setupUI() {
        configNavigationBar()
        tableView.dataSource = dataSource
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        
        appDelegate.locationManager.delegate = self
        appDelegate.locationManager.distanceFilter = 0
    }
}

extension DetailViewController {
    private func configNavigationBar() {
        self.title = "站點地圖"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "開始導航", style: .plain, target: self, action: #selector(navigationRoute))
    }
}

extension DetailViewController {
    @objc func navigationRoute() {
        let targetCoordinate = CLLocationCoordinate2D(latitude: data?.lat.toDouble() ?? 0, longitude: data?.lng.toDouble() ?? 0)
        let targetPlacemark = MKPlacemark(coordinate: targetCoordinate)
        let targetMapItem = MKMapItem(placemark: targetPlacemark)
        let userMapItem = MKMapItem.forCurrentLocation()
        let routes = [userMapItem, targetMapItem]
        MKMapItem.openMaps(with: routes, launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
}

extension DetailViewController {
    enum Section {
        case map
        case info
    }
    
    enum Item: Hashable {
        case map(DetailMapViewData)
        case info(DetailInfoViewData)
    }
    
    private func makeDataSource() -> UITableViewDiffableDataSource<Section, Item> {
        return UITableViewDiffableDataSource(tableView: tableView) { tableView, indexPath, item in
            switch item {
            case .map(let model):
                let cell = tableView.dequeueReusableCell(with: DetailMapTableViewCell.self, for: indexPath)
                cell.configure(viewData: model)
                return cell
            case .info(let model):
                let cell = tableView.dequeueReusableCell(with: DetailInfoTableViewCell.self, for: indexPath)
                cell.configure(viewData: model)
                return cell
            }
            
        }
    }
    
    private func initDataSource(animate: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Item>()
        snapshot.appendSections([.map, .info])
        let mapItem = Item.map(DetailMapViewData(data: data))
        let infoItem = Item.info(DetailInfoViewData(data: data))
        snapshot.appendItems([mapItem], toSection: .map)
        snapshot.appendItems([infoItem], toSection: .info)
        
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}

extension DetailViewController:  CLLocationManagerDelegate  {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        appDelegate.locationManager.startUpdatingLocation()
    }
}
