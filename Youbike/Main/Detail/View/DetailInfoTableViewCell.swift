//
//  DetailInfoTableViewCell.swift
//  Youbike
//
//  Created by 季紅 on 2021/12/6.
//

import UIKit
import MapKit
import CoreLocation

struct DetailInfoViewData: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(data?.sno)
    }
    
    static func == (lhs: DetailInfoViewData, rhs: DetailInfoViewData) -> Bool {
        lhs.data?.sno == rhs.data?.sno
    }
    
    let data: YoubikeInfo?
}

class DetailInfoTableViewCell: UITableViewCell, ConfigCell {
    
    typealias ViewData = DetailInfoViewData
    
    lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var totalCountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 2
        label.textAlignment = .left
        return label
    }()
    
    lazy var updateTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(viewData: DetailInfoViewData) {
        guard let data = viewData.data else {
            return
        }
        addressLabel.text = "\(data.sna)\n\(data.ar)"
        totalCountLabel.text = "可停:\(data.bemp.toInt() ?? 0) 台\n可借:\(data.sbi.toInt() ?? 0) 台"
        updateTimeLabel.text = "更新時間:\(data.mday.stringConvertDate().dateConvertString())"
    }
}

extension DetailInfoTableViewCell {
    func setupUI() {
        self.selectionStyle = .none
        self.contentView.addSubview(addressLabel)
        addressLabel.snp.makeConstraints({
            $0.top.equalToSuperview().offset(15)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
        })
        self.contentView.addSubview(totalCountLabel)
        totalCountLabel.snp.makeConstraints({
            $0.top.equalTo(addressLabel.snp.bottom).offset(15)
            $0.left.right.equalTo(addressLabel)
        })
        self.contentView.addSubview(updateTimeLabel)
        updateTimeLabel.snp.makeConstraints({
            $0.top.equalTo(totalCountLabel.snp.bottom).offset(15)
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview().offset(-15)
        })
    }
}
