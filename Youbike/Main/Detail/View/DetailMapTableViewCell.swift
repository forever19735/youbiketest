//
//  DetailMapTableViewCell.swift
//  Youbike
//
//  Created by 季紅 on 2021/12/6.
//

import UIKit
import MapKit
import CoreLocation

struct DetailMapViewData: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(data?.sno)
    }
    
    static func == (lhs: DetailMapViewData, rhs: DetailMapViewData) -> Bool {
        lhs.data?.sno == rhs.data?.sno
    }
    
    let data: YoubikeInfo?
}

class DetailMapTableViewCell: UITableViewCell, ConfigCell {
    
    typealias ViewData = DetailMapViewData
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.showsUserLocation = true
        mapView.delegate = self
        return mapView
    }()
    
    var data: YoubikeInfo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func configure(viewData: DetailMapViewData) {
        self.data = viewData.data
        addAnnotation()
    }
    
}

extension DetailMapTableViewCell {
    func setupUI() {
        self.contentView.addSubview(mapView)
        mapView.snp.makeConstraints({
            $0.top.leading.bottom.trailing.equalToSuperview()
            $0.height.equalTo(500)
        })
    }
    
    private func updateLocation() {
        let range: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let lat = data?.lat.toDouble() ?? 0
        let lng = data?.lng.toDouble() ?? 0
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let appearRegion: MKCoordinateRegion = MKCoordinateRegion(center: location, span: range)
        
        mapView.setRegion(appearRegion, animated: true)
    }
    
    private func addAnnotation() {
        let objectAnnoutation = MKPointAnnotation()
        let lat = data?.lat.toDouble() ?? 0
        let lng = data?.lng.toDouble() ?? 0
        objectAnnoutation.coordinate = CLLocation(latitude: lat, longitude: lng).coordinate
        objectAnnoutation.title = data?.sna
        mapView.addAnnotation(objectAnnoutation)
    }
}

extension DetailMapTableViewCell: MKMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        updateLocation()
    }
}
