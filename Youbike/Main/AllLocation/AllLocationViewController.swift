//
//  AllLocationViewController.swift
//  Youbike
//
//  Created by johnlin on 2021/3/17.
//

import UIKit
import MapKit
import CoreLocation

class AllLocationViewController: BaseViewController {
    var mapView: MKMapView!
    
    var currentLocationButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "icon_current_location"), for: .normal)
        return button
    }()
    
    var currentLocation: CLLocationCoordinate2D?
    
    var viewModel = YoubikeViewModel()
    
    init(viewModel: YoubikeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appDelegate.startLocationManager()
    }
    
    override func setupUI() {
        appDelegate.locationManager.delegate = self
        appDelegate.locationManager.distanceFilter = 1000.0
        configMapView()
        configCurrentLocation()
    }
    
    override func bind() {
        currentLocationButton
            .publisher(for: .touchUpInside)
            .sink { [weak self] (_) in
                self?.showCurrentLocation()
            }.store(in: &cancellables)
        viewModel.$data
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (data) in
                self?.initAnnotation(infos: data)
            }.store(in: &cancellables)
    }
    
}

extension AllLocationViewController {
    private func configMapView() {
        mapView = MKMapView()
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        self.view.addSubview(mapView)
        mapView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
    }
    
    private func configCurrentLocation() {
        self.view.addSubview(currentLocationButton)
        currentLocationButton.snp.makeConstraints({
            $0.trailing.equalToSuperview().offset(-20)
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-20)
            $0.width.height.equalTo(50)
        })
        currentLocationButton.layer.cornerRadius = currentLocationButton.frame.size.height / 2
        currentLocationButton.layer.masksToBounds = true
    }
    
    private func showCurrentLocation() {
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        guard let center = currentLocation else {
            return
        }
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    private func initAnnotation(infos: [YoubikeInfo]) {
        infos.forEach({
            let pin = MKPointAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: $0.lat.toDouble() ?? 0, longitude: $0.lng.toDouble() ?? 0)
            pin.title = $0.sna
            pin.subtitle = "可停:\($0.bemp.toInt() ?? 0) 台;可借:\($0.sbi.toInt() ?? 0) 台"
            DispatchQueue.main.async {
                self.mapView.addAnnotation(pin)
            }
        })
    }
}

extension AllLocationViewController {
    
}

extension AllLocationViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0].coordinate
        let range: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let location = CLLocationCoordinate2D(latitude: currentLocation!.latitude, longitude: currentLocation!.longitude)
        let appearRegion: MKCoordinateRegion = MKCoordinateRegion(center: location, span: range)
        
        mapView.setRegion(appearRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "AnnotationIdentifier"
        if annotation is MKUserLocation { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let center = view.annotation?.coordinate else {
            return
        }
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: true)
    }
}
