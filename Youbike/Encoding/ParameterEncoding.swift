//
//  ParameterEncoding.swift
//  Youbike
//
//  Created by johnlin on 2021/3/16.
//

import Foundation

public typealias CustomParameters = [String: Any]

public protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: CustomParameters) throws
}
